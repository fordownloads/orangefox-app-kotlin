package com.fordownloads.orangefox.utils

import android.view.View
import android.view.ViewGroup
import androidx.core.graphics.Insets
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.updateLayoutParams
import com.fordownloads.orangefox.base.IBase

fun IBase.applyInsets(root: View, toolbar: View? = null, recycler: View? = null, fab: View? = null,
                      fabMargin: Int = 24, recyclerPadding: Boolean = true,
                      init: (Insets.() -> Unit)? = null) {
    ViewCompat.setOnApplyWindowInsetsListener(root) { _, windowInsets ->
        with (windowInsets.getInsets(WindowInsetsCompat.Type.systemBars())) {
            toolbar?.updateLayoutParams<ViewGroup.MarginLayoutParams> {
                topMargin = top
                leftMargin = left
                rightMargin = right
            }

            recycler?.updateLayoutParams<ViewGroup.MarginLayoutParams> {
                leftMargin = left
                rightMargin = right

                if (recyclerPadding)
                    recycler.setPadding(0, 0, 0, dp(fabMargin * 2 + 40) + bottom)
            }

            fab?.updateLayoutParams<ViewGroup.MarginLayoutParams> {
                val margin = dp(fabMargin)
                leftMargin = left + margin
                rightMargin = right + margin
                bottomMargin = bottom + margin
            }

            if (init != null) init()

            WindowInsetsCompat.CONSUMED
        }
    }
}