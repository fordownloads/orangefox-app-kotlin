package com.fordownloads.orangefox.utils

import android.content.Context
import android.graphics.Canvas
import android.graphics.Path
import android.graphics.Rect
import androidx.recyclerview.widget.RecyclerView
import android.graphics.RectF

import androidx.annotation.DimenRes
import androidx.core.view.children
import androidx.recyclerview.widget.RecyclerView.ItemDecoration
import kotlin.math.max
import kotlin.math.min


class RoundedDecor(radius: Float) : ItemDecoration() {
    private val defaultRectToClip = RectF(Float.MAX_VALUE, Float.MAX_VALUE, 0f, 0f)
    private val bottomRounded = floatArrayOf(0f, 0f, 0f, 0f, radius, radius, radius, radius)

    override fun onDraw(canvas: Canvas, parent: RecyclerView, state: RecyclerView.State) {
        val roundRect = RectF(defaultRectToClip)
        val childRect = Rect()
        parent.children.forEach {
            parent.getDecoratedBoundsWithMargins(it, childRect)
            roundRect.left = min(roundRect.left,     childRect.left.toFloat())
            roundRect.top =  min(roundRect.top,       childRect.top.toFloat())
            roundRect.right = max(roundRect.right,   childRect.right.toFloat())
            roundRect.bottom = max(roundRect.bottom, childRect.bottom.toFloat())
        }

        if (roundRect == defaultRectToClip)
            return

        canvas.clipPath(Path().apply {
            addRoundRect(roundRect, bottomRounded, Path.Direction.CW)
        })
    }

}