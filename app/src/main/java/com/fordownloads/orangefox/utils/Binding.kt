package com.fordownloads.orangefox.utils

import android.animation.LayoutTransition
import android.view.ContextThemeWrapper
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TableLayout
import android.widget.TableRow
import android.widget.TextView
import androidx.core.view.updateLayoutParams
import androidx.databinding.BindingAdapter
import com.fordownloads.orangefox.R

@BindingAdapter("layout_marginBottom")
fun setLayoutMarginBottom(view: View, dimen: Int) {
    view.updateLayoutParams<ViewGroup.MarginLayoutParams> {
        bottomMargin = (view.context.resources.displayMetrics.density * dimen).toInt()
    }
}

@BindingAdapter("layout_marginHorizontal")
fun setLayoutMarginHorizontal(view: View, dimen: Int) {
    val padding = (view.context.resources.displayMetrics.density * dimen).toInt()
    view.updateLayoutParams<ViewGroup.MarginLayoutParams> {
        leftMargin = padding
        rightMargin = padding
    }
}

@BindingAdapter("visible")
fun setVisible(view: View, value: Boolean) {
    view.visibility = if (value) View.VISIBLE else View.GONE
}

@BindingAdapter("clipToOutline")
fun setClipToOutline(view: View, value: Boolean) {
    view.clipToOutline = value
}

@BindingAdapter("animated")
fun setAnimated(view: ViewGroup, value: Boolean = false) {
    if (value)
        view.layoutTransition = LayoutTransition().apply {
            disableTransitionType(LayoutTransition.CHANGING)
            disableTransitionType(LayoutTransition.CHANGE_DISAPPEARING)
            disableTransitionType(LayoutTransition.CHANGE_APPEARING)

        }
}

@BindingAdapter("table")
fun setTable(table: TableLayout, content: Map<Int, String?>) {
    with (table.context) {
        table.removeAllViews()
        lateinit var row: TableRow
        content.entries.forEachIndexed { index, entry ->
            if (index % 2 == 0) {
                row = TableRow(ContextThemeWrapper(this@with, R.style.TableRow))
                table.addView(row)
            }
            entry.value?.let {
                row.addView(ImageView(ContextThemeWrapper(this@with, R.style.TableIcon), null, 0).apply { setImageResource(entry.key)})
                row.addView(TextView(ContextThemeWrapper(this@with, R.style.Text_Bold)).apply { text = it })
            }
        }
    }
}