package com.fordownloads.orangefox.ui.install

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.updateLayoutParams
import androidx.recyclerview.widget.RecyclerView
import com.fordownloads.orangefox.databinding.ItemCardBinding
import kotlin.math.max


data class CardItem(val title: String, val table: Map<Int, String?>, val infoListener: View.OnClickListener)

data class CardAdapterInstall(
    private val items: ArrayList<CardItem>
) : RecyclerView.Adapter<CardAdapterInstall.ViewHolder>() {

    inner class ViewHolder(val b: ItemCardBinding) : RecyclerView.ViewHolder(b.root)

    override fun getItemCount() = items.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewHolder(ItemCardBinding.inflate(LayoutInflater.from(parent.context), parent, false).apply {
            root.layoutParams = ViewGroup.LayoutParams((parent.width * 0.85).toInt(),ViewGroup.LayoutParams.MATCH_PARENT)
        })

    override fun onBindViewHolder(holder: ViewHolder, pos: Int) {
        holder.b.item = items[pos]
    }
}