package com.fordownloads.orangefox.ui.info

import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.PagerAdapter

class PagerAdapterInfo(var views: List<Pair<String, View>>) :
    PagerAdapter() {

    override fun getPageTitle(position: Int) = views[position].first
    override fun getCount() = views.size
    override fun isViewFromObject(view: View, obj: Any) = view === obj
    override fun destroyItem(container: ViewGroup, position: Int, obj: Any) = container.removeView(views[position].second)

    override fun instantiateItem(container: ViewGroup, position: Int) =
        views[position].second.apply {
            container.addView(this)
        }

    override fun getItemPosition(obj: Any): Int {
        for (index in 0 until count)
            if (obj as View === views[index])
                return index
        return POSITION_NONE
    }
}