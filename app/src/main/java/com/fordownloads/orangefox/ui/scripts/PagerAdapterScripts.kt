package com.fordownloads.orangefox.ui.scripts

import android.app.Activity
import android.content.Intent
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.ActivityResultLauncher
import androidx.viewpager.widget.PagerAdapter
import com.fordownloads.orangefox.R
import com.fordownloads.orangefox.RecyclerItem
import java.lang.IllegalStateException

class PagerAdapterScripts(private val activity: Activity,
                          private val addZipFunction: View.OnClickListener,
                          addCallback: (item: RecyclerItem) -> Unit) : PagerAdapter() {

    override fun getCount() = 6
    override fun isViewFromObject(view: View, obj: Any) = view == obj
    override fun destroyItem(container: ViewGroup, position: Int, obj: Any) =
        container.removeView(obj as View)

    private val create = ScriptPagesCreator(activity, addCallback)

    override fun instantiateItem(vg: ViewGroup, pos: Int): View {
        val v = when (pos) {
            0 -> create.installTab(vg, addZipFunction)
            1 -> create.backupTab(vg)
            2 -> create.wipeTab(vg)
            3 -> create.mountTab(vg)
            4 -> create.rebootTab(vg)
            5 -> create.etcTab(vg)
            else -> throw IllegalStateException()
        }
        vg.addView(v,0)
        return v
    }

    override fun getPageTitle(position: Int) =
        activity.getString(when (position) {
            0    -> R.string.script_zip
            1    -> R.string.script_backup
            2    -> R.string.script_wipe
            3    -> R.string.script_mount
            4    -> R.string.script_reboot
            5    -> R.string.script_etc
            else -> throw IllegalStateException()
        })
}