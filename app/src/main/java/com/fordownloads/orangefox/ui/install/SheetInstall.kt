package com.fordownloads.orangefox.ui.install

import android.Manifest.permission.*
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.view.View.OnClickListener
import androidx.appcompat.app.AppCompatActivity
import com.fordownloads.orangefox.App
import com.fordownloads.orangefox.Const
import com.fordownloads.orangefox.R
import com.fordownloads.orangefox.ReleaseToInstall
import com.fordownloads.orangefox.base.SheetBase
import com.fordownloads.orangefox.databinding.ButtonsInstallBinding
import com.fordownloads.orangefox.updates.DownloadService
import com.fordownloads.orangefox.utils.Tools.checkMD5
import com.fordownloads.orangefox.utils.Tools.getORS
import com.fordownloads.orangefox.utils.Tools.showSnackbar
import com.topjohnwu.superuser.Shell
import java.io.File

object SheetInstall {
    @SuppressLint("StringFormatMatches")
    fun AppCompatActivity.createInstallSheet(
        release: ReleaseToInstall,
        force: Boolean = false,
        finishActivity: Boolean = false
    ): SheetBase {
        val finalFile = File(Const.DIR_DOWNLOAD, release.variant.filename)
        val fileExist = !force && finalFile.exists()

        var sheet = SheetBase(this)

        sheet.body.apply {
            title = getString(R.string.install_latest, release.version, release.type)
            secondTitle = getString(R.string.rel_variant_title, release.variant.variant_name)
            subtitle = getString(if (fileExist) R.string.inst_file_found else R.string.inst_warn)
        }

        ButtonsInstallBinding.inflate(layoutInflater, sheet.body.inner, true).apply {
            exist = fileExist
            if (fileExist) {
                installTap = OnClickListener {
                    if (Shell.rootAccess()) {
                        if (checkMD5(release.variant.md5, finalFile)) {
                            if (Shell.su("echo \"install /sdcard/Fox/releases/${release.variant.filename}\" > ${getORS()}")
                                    .exec().isSuccess
                            ) {
                                if (!Shell.su("reboot recovery").exec().isSuccess)
                                    showSnackbar(R.string.err_reboot_notify, sheet.dialog)
                            } else showSnackbar(R.string.err_ors_short, sheet.dialog)
                        } else showSnackbar(R.string.err_md5_wrong_exists, sheet.dialog)
                    } else showSnackbar(R.string.err_no_pm_root, sheet.dialog)
                }
                downloadTap = OnClickListener {
                    sheet.dismiss()
                    sheet = createInstallSheet(release, true, finishActivity)
                    sheet.show()
                }
                deleteTap = OnClickListener {
                    if (finalFile.delete())
                        sheet.dismiss()
                    else
                        showSnackbar(R.string.err_file_delete, sheet.dialog)
                }
            } else {
                OnClickListener {
                    if (it.id == R.id.download || Shell.rootAccess()) {
                        if ((application as App).isDownloadServiceRunning())
                            showSnackbar(R.string.err_service_running, sheet.dialog)
                        else {
                            if (finishActivity) {
                                setResult(Activity.RESULT_OK, null)
                                finish()
                            }
                            showSnackbar(R.string.inst_bg_download, sheet.dialog)
                            startService(
                                Intent(this@createInstallSheet, DownloadService::class.java)
                                    .putExtra(Const.EXTRA_RELEASE, release)
                                    .putExtra(Const.EXTRA_RELEASE_INSTALL, it.id == R.id.install)
                            )
                        }
                    } else showSnackbar(R.string.err_no_pm_root, sheet.dialog)
                }.let { installTap = it; downloadTap = it }
            }
        }

        return sheet
    }
}