package com.fordownloads.orangefox.ui.scripts


import android.app.Activity
import com.fordownloads.orangefox.R
import com.fordownloads.orangefox.RecyclerItem
import com.fordownloads.orangefox.utils.Tools.cap
import com.topjohnwu.superuser.io.SuFileInputStream
import java.io.File
import java.io.IOException
import java.util.*


class ScriptParser(val context: Activity) {
    private val noArgs = context.getString(R.string.script_no_params)
    private val backupList = context.resources.getStringArray(R.array.scripts_backup).map { "$it, " }
    private val rebootList = context.resources.getStringArray(R.array.scripts_reboot)
    var hasErrors = false

    @Throws(IOException::class)
    fun parse(file: File): ArrayList<RecyclerItem> {
        val items = ArrayList<RecyclerItem>()
        SuFileInputStream.open(file).bufferedReader().forEachLine { l ->
            processLine(l)?.let { items.add(it) }
        }
        return items
    }

    private fun processLine(line: String): RecyclerItem? {
        val split = line.split(' ')
        val arg1 = if (split.size == 1) "" else split[1]
        val arg2 = if (split.size == 3) split[2] else ""

        fun i(str: Int, icon:Int, sub: String = arg1) = RecyclerItem(context.getString(str), sub, icon, line)

        fun e(str: Int, icon:Int, sub: String = arg1): RecyclerItem {
            if (sub.isEmpty()) hasErrors = true
            return RecyclerItem(context.getString(str), sub, icon, line)
        }

        return when (split[0]) {
            "" -> null
            "install"    -> e(R.string.script_zip,      R.drawable.ic_archive)
            "backup"     -> e(R.string.script_backup,   R.drawable.ic_backup, createPartitionsString(arg1, arg2))
            "restore"    -> e(R.string.script_restore,  R.drawable.ic_restore,         createPartitionsString(arg2, arg1))
            "decrypt"    -> e(R.string.script_decrypt,  R.drawable.ic_lock)
            "cmd"        -> e(R.string.script_cmd,      R.drawable.ic_terminal)
            "mkdir"      -> e(R.string.script_mkdir,    R.drawable.ic_folder)
            "print"      -> e(R.string.script_print,    R.drawable.ic_message)
            "wipe"       -> e(R.string.script_wipe,     R.drawable.ic_delete,          arg1.cap())
            "mount"      -> e(R.string.script_mount,    R.drawable.ic_mount,  arg1.cap())
            "set"        -> e(R.string.script_set,      R.drawable.ic_equals, "$arg1 = $arg2")
            "sideload"   -> i(R.string.script_sideload, R.drawable.ic_sideload, noArgs)
            "listmounts" -> i(R.string.script_list_mnt, R.drawable.ic_message,  noArgs)
            "remountrw"  -> i(R.string.script_rw,       R.drawable.ic_remount, "System")
            "unmount",
            "umount"     -> e(R.string.script_umount,   R.drawable.ic_umount, arg1.cap())
            "fixpermissions",
            "fixcontexts",
            "fixperms"   -> i(R.string.script_fixperms, R.drawable.ic_wrench, noArgs)
            "reboot" ->     i(R.string.script_reboot,   R.drawable.ic_refresh,
                when (arg1) {
                    "" -> rebootList[0]
                    "recovery" -> rebootList[1]
                    "poweroff" -> rebootList[2]
                    "bootloader" -> rebootList[3]
                    "download" -> rebootList[4]
                    "edl" -> rebootList[5]
                    else -> arg1.cap()
            })
            else -> i(R.string.unknown, R.drawable.ic_close, line)
        }
    }

    private fun createPartitionsString(letters: String, name: String): String {
        if (letters.isEmpty() && name.isEmpty()) return noArgs
        var parts = if (name.isNotEmpty()) "${name}\n" else ""
        var options = ""

        for (part in letters)
            when (part) {
                's', 'S' -> parts += backupList[3]
                'd', 'D' -> parts += backupList[2]
                'c', 'C' -> parts += backupList[4]
                'r', 'R' -> parts += backupList[1]
                'b', 'B' -> parts += backupList[0]
                'a', 'A' -> parts += "Android Secure, "
                'e', 'E' -> parts += "SD-EXT, "
                'o', 'O' -> options += "${context.getString(R.string.backup_optimize)}\n"
                'm', 'M' -> options += "${context.getString(R.string.backup_digest)}\n"
            }
        return "${parts.trim(' ', ',')}\n${options}".trim()
    }
}