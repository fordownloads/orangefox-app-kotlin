package com.fordownloads.orangefox.ui.settings

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import com.fordownloads.orangefox.Const.argStandalone
import com.fordownloads.orangefox.Prefs
import com.fordownloads.orangefox.R
import com.fordownloads.orangefox.base.IBase
import com.fordownloads.orangefox.ui.setup.FragmentDevices
import com.fordownloads.orangefox.utils.Tools.openUrl
import com.fordownloads.orangefox.utils.Tools.replace
import com.fordownloads.orangefox.utils.applyInsets
import com.google.android.material.appbar.MaterialToolbar
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton

class FragmentSettings : PreferenceFragmentCompat(), IBase {
    override fun dp (i: Int) = ((resources.displayMetrics?.density?:1F) * i).toInt()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, state: Bundle?) =
        super.onCreateView(inflater, container, state).apply {
            applyInsets(this,
                recycler = findViewById<FrameLayout>(android.R.id.list_container),
                toolbar = findViewById<MaterialToolbar>(R.id.appToolbar).apply {
                    setNavigationOnClickListener { activity?.finish() }
                },
                fab = findViewById<ExtendedFloatingActionButton>(R.id.feedback).apply {
                    setOnClickListener { activity?.openUrl("https://t.me/OrangeFoxApp") }
                }
            )
        }

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.root_preferences, rootKey)
        val fm = activity?.supportFragmentManager
        findPreference<Preference>("change_device")?.apply {
            summary = Prefs(sharedPreferences ?: run { throw Error("Prefs creation error") }).deviceName
            setOnPreferenceClickListener {
                fm?.replace(FragmentDevices().apply { argStandalone = true }); true
            }
        }
        findPreference<Preference>("about")?.setOnPreferenceClickListener {
            fm?.replace(AboutFragment()); true
        }
    }
}