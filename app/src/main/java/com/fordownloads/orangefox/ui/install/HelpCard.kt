package com.fordownloads.orangefox.ui.install

import android.view.View

data class HelpCard(val title: String, val subtitle: String, var important: Boolean = false, val listener: View.OnClickListener? = null)