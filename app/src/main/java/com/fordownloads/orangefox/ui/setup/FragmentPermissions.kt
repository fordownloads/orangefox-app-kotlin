package com.fordownloads.orangefox.ui.setup

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import com.fordownloads.orangefox.BuildConfig
import com.fordownloads.orangefox.Const
import com.fordownloads.orangefox.Const.argStandalone
import com.fordownloads.orangefox.R
import com.fordownloads.orangefox.base.FragmentBase
import com.fordownloads.orangefox.databinding.FragmentPermissionsBinding
import com.fordownloads.orangefox.utils.Tools.activityCallback
import com.fordownloads.orangefox.utils.Tools.hasStoragePermission
import com.fordownloads.orangefox.utils.Tools.hasStoragePermissionScoped
import com.fordownloads.orangefox.utils.applyInsets
import com.topjohnwu.superuser.Shell

class FragmentPermissions: FragmentBase<FragmentPermissionsBinding>(R.layout.fragment_permissions, skipYeet = true) {
    private val manualPermissionSet = activityCallback { checkStatus() }
    private val storageCallback = registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) { checkStatus() }

    private fun updateStatus() {
        val storagePM = requireActivity().hasStoragePermission() && hasStoragePermissionScoped()
        b.storageGranted = storagePM
        if (storagePM) b.next.show()
    }

    @SuppressLint("InlinedApi")
    private fun checkStatus() {
        when {
            !requireActivity().hasStoragePermission() -> Toast.makeText(activity, R.string.help_deny_pm, Toast.LENGTH_LONG).show()
            hasStoragePermissionScoped() -> updateStatus()
            else -> {
                Toast.makeText(activity, R.string.help_android11_pm, Toast.LENGTH_LONG).show()
                settingsCallback.launch(Intent(Settings.ACTION_MANAGE_ALL_FILES_ACCESS_PERMISSION))
            }
        }
    }

    private val settingsCallback = activityCallback {
        if (hasStoragePermissionScoped())
            updateStatus()
        else
            Toast.makeText(activity, R.string.help_deny_pm, Toast.LENGTH_LONG).show()
    }

    override fun setup(state: Bundle?) {
        applyInsets(b.root, toolbar = b.appToolbar, recycler = b.scroll, fab = b.next, fabMargin = 36)

        if (argStandalone) {
            b.appToolbar.setPadding(0, 0, dp(16), 0)
            b.appToolbar.navigationIcon = null
            b.next.setOnClickListener {
                activity?.apply {
                    InitSetup.cleanup()
                    prefs.setupCompleted = true
                    startActivity(intent.apply {
                        addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        addCategory(Intent.CATEGORY_HOME)
                        addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    })
                }
            }
        } else {
            b.appToolbar.setNavigationOnClickListener { activity?.onBackPressed() }
            b.next.setOnClickListener {
                activity?.apply {
                    InitSetup.cleanup()
                    prefs.setupCompleted = true
                    startActivity(intent.apply {
                        addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        addCategory(Intent.CATEGORY_HOME)
                        addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    })
                }
            }
        }

        b.grantRoot.setOnClickListener {
            if (Shell.rootAccess())
                b.rootGranted = true
            else
                Toast.makeText(activity, R.string.err_no_pm_root, Toast.LENGTH_LONG).show()
        }

        b.grantStorage.setOnClickListener {
            when {
                // user can go to settings and enable pm + it can avoid bugs
                requireActivity().hasStoragePermission() && hasStoragePermissionScoped() ->
                    updateStatus()

                !requireActivity().hasStoragePermission() -> {
                    if (!shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE))
                        storageCallback.launch(Const.STORAGE_PM)
                    else {
                        Toast.makeText(activity, R.string.help_storage_pm, Toast.LENGTH_LONG).show()
                        manualPermissionSet.launch(Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS).apply {
                            data = Uri.fromParts("package", BuildConfig.APPLICATION_ID, null)
                        })
                    }
                }

                !hasStoragePermissionScoped() -> {
                    Toast.makeText(activity, R.string.help_android11_pm, Toast.LENGTH_LONG).show()
                    settingsCallback.launch(Intent(Settings.ACTION_MANAGE_ALL_FILES_ACCESS_PERMISSION))
                }
            }
        }

        updateStatus()
    }
}