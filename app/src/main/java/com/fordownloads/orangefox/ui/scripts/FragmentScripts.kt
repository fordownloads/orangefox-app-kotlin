package com.fordownloads.orangefox.ui.scripts

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.*
import androidx.recyclerview.widget.ItemTouchHelper
import com.fordownloads.orangefox.*
import com.fordownloads.orangefox.base.FragmentBase
import com.fordownloads.orangefox.databinding.FragmentScriptsBinding
import com.fordownloads.orangefox.databinding.SheetPagesBinding
import com.fordownloads.orangefox.base.SheetBase
import com.fordownloads.orangefox.utils.RoundedDecor
import com.fordownloads.orangefox.utils.Tools
import com.fordownloads.orangefox.utils.Tools.activityCallback
import com.fordownloads.orangefox.utils.Tools.getORS
import com.fordownloads.orangefox.utils.Tools.reportException
import com.topjohnwu.superuser.Shell
import com.topjohnwu.superuser.io.SuFile
import java.io.IOException


class FragmentScripts : FragmentBase<FragmentScriptsBinding>(R.layout.fragment_scripts) {
    private lateinit var adapter: AdapterScripts
    private lateinit var touchHelper: ItemTouchHelper
    private lateinit var parser: ScriptParser

    private lateinit var sheet: SheetBase
    private var items = ArrayList<RecyclerItem>()

    private val addZipCallback = activityCallback {
        if (it.resultCode != Activity.RESULT_OK) return@activityCallback

        val path = it.data?.let { data -> Tools.getFileFromFilePicker(data) }
        sheet.dismiss()
        if (path == null)
            showSnackbar(R.string.err_filepicker)
        else
            add(RecyclerItem(getString(R.string.script_zip), path,
                      R.drawable.ic_archive, "install $path"))
    }

    private val addZipFunction = View.OnClickListener {
        addZipCallback.launch(
            Intent(Intent.ACTION_OPEN_DOCUMENT).apply {
                addCategory(Intent.CATEGORY_OPENABLE)
                type = "application/zip"
            })
    }

    override fun onSaveInstanceState(state: Bundle) {
        super.onSaveInstanceState(state.apply { putParcelableArrayList("items", items) })
    }

    fun add(i: RecyclerItem) {
        items.add(i)
        adapter.notifyItemInserted(items.size - 1)
        sheet.dismiss()
        b.items = items
    }

    override fun setup(state: Bundle?) {
        sheet = SheetBase(requireActivity())

        sheet.body.apply {
            title = getString(R.string.script_add_title)

            SheetPagesBinding.inflate(layoutInflater, inner, true).apply {
                pager.offscreenPageLimit = 8
                pager.adapter = PagerAdapterScripts(requireActivity(), addZipFunction) { add(it) }
                tabs.setupWithViewPager(pager)
            }
        }
        state?.getParcelableArrayList<RecyclerItem>("items")?.let { items = it }

        b.apply {
            setAdd { sheet.show() }
            setCreate { buildScript(getORS()) }
            items = this@FragmentScripts.items
            recycler.addItemDecoration(RoundedDecor(dpF(24)))
        }

        activity?.let { parser = ScriptParser(it) }

        adapter = AdapterScripts(
            items = items,
            clickListener = null,
            dragStartListener = { it?.let { touchHelper.startDrag(it) } },
            dismissListener = { b.invalidateAll() })

        touchHelper = ItemTouchHelper(AdapterScriptsTouchHelper(adapter)).apply {
            attachToRecyclerView(b.recycler)
        }

        b.recycler.apply {
            setHasFixedSize(true)
            adapter = this@FragmentScripts.adapter
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.clear()
        inflater.inflate(R.menu.scripts, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.clear -> {
                items.clear()
                adapter.notifyDataSetChanged()
                b.invalidateAll()
            }
            R.id.load -> {
                if (!Shell.rootAccess()) {
                    showSnackbar(R.string.err_no_pm_root)
                    return false
                }
                val orsFile = SuFile.open(getORS())
                if (!orsFile.exists()) {
                    showSnackbar(R.string.err_no_ors)
                    return false
                }
                items.clear()
                try {
                    items.addAll(parser.parse(orsFile))
                    adapter.notifyDataSetChanged()
                    b.invalidateAll()
                    showSnackbar(
                        if (parser.hasErrors)
                            R.string.err_ors_bad
                        else
                            R.string.parser_ok
                    )
                } catch (e: IOException) {
                    reportException(e)
                    showSnackbar(R.string.err_ors_bad)
                }
            }
            R.id.delete -> {
                if (Shell.rootAccess()) {
                    val orsFile = SuFile.open(getORS())
                    if (!orsFile.exists())
                        showSnackbar(R.string.err_no_ors)
                    else if (orsFile.delete())
                        showSnackbar(R.string.deleted_ors)
                    else
                        showSnackbar(R.string.err_file_delete)
                } else
                    showSnackbar(R.string.err_no_pm_root)
            }
            R.id.reboot -> {
                if (!Shell.su("reboot recovery").exec().isSuccess)
                    showSnackbar(R.string.err_reboot_notify)
            }
            R.id.rebootBootloader -> {
                if (!Shell.su("reboot bootloader").exec().isSuccess)
                    showSnackbar(R.string.err_reboot_notify)
            }
        }
        return false
    }

    private fun buildScript(orsFile: String, force: Boolean = false, silent: Boolean = false) {
        if (items.size == 0) {
            if (!silent)
                showSnackbar(R.string.script_err_nothing)
            return
        }
        if (!Shell.rootAccess()) {
            if (!silent)
                showSnackbar(R.string.err_no_pm_root)
            return
        }
        var content = "echo \""
        for (item in items)
            content += "${item.data}\n"
        if (!force && content.contains("wipe system") && !content.contains("install ")) {
            getSnackbar(R.string.script_err_rom)?.setAction(R.string.ignore) {
                buildScript(getORS(), force = true, silent = false)
            }?.show()
            return
        }
        content += "\" > $orsFile"
        if (Shell.su(content.trim { it <= ' ' }).exec().isSuccess && !silent)
            getSnackbar(R.string.script_created)?.setAction(R.string.reboot) {
                if (!Shell.su("reboot recovery").exec().isSuccess)
                    showSnackbar(R.string.err_reboot_notify)
            }?.show()
        else if (!silent)
            showSnackbar(R.string.err_ors_short)
    }
}