package com.fordownloads.orangefox.ui.setup

import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import androidx.core.view.updateLayoutParams
import com.fordownloads.orangefox.*
import com.fordownloads.orangefox.api.DeviceRequest
import com.fordownloads.orangefox.api.RequestAPI.orangeFoxApi
import com.fordownloads.orangefox.base.FragmentBase
import com.fordownloads.orangefox.databinding.*
import com.fordownloads.orangefox.utils.Tools
import com.fordownloads.orangefox.utils.Tools.isLandscape
import com.fordownloads.orangefox.utils.Tools.replace
import com.fordownloads.orangefox.utils.Tools.startAnimation
import com.fordownloads.orangefox.utils.applyInsets
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.UnknownHostException


class FragmentWelcome: FragmentBase<FragmentWelcomeBinding>(R.layout.fragment_welcome, skipYeet = true) {
    override fun setup(state: Bundle?) {
        applyInsets(b.root, fab = b.bottom) {
            b.nextLandscape.updateLayoutParams<ViewGroup.MarginLayoutParams> {
                val max = Integer.max(left, right) + dp(24)
                leftMargin = max
                rightMargin = max
            }
            if (b.loaded)
                if (isLandscape()) {
                    b.landscape = true
                    b.next.hide()
                    b.nextLandscape.show()
                } else {
                    b.landscape = false
                    b.next.show()
                    b.nextLandscape.hide()
                }
        }
        b.landscape = isLandscape()

        orangeFoxApi.devicesAll().enqueue(object : Callback<DeviceRequest> {
            override fun onFailure(call: Call<DeviceRequest>, t: Throwable) {
                Tools.reportException(t, false)
                b.errorMessage = if (t is UnknownHostException) getString(R.string.err_no_internet)
                                 else t.toString()
            }

            override fun onResponse(call: Call<DeviceRequest>, response: Response<DeviceRequest>) {
                if (response.isSuccessful)
                    response.body()?.let { parseDeviceList(it) }
                else
                    b.errorMessage = "${response.code()}: ${getString(R.string.err_ise)}"
            }
        })
    }

    fun parseDeviceList(response: DeviceRequest) {
        InitSetup.devices = InitSetup.listDevices(response) ?: run {
            b.errorMessage = getString(R.string.err_ise)
            return
        }

        val go = View.OnClickListener { fm?.replace(FragmentDevices(), backAllowed = false) }
        b.nextLandscape.setOnClickListener(go)
        b.next.setOnClickListener(go)

        b.loading.startAnimation()
        b.loaded = true

        if (isLandscape())
            b.nextLandscape.show()
        else
            b.next.show()
    }
}
