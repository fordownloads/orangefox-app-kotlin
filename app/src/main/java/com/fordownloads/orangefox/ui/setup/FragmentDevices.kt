package com.fordownloads.orangefox.ui.setup

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.ViewGroup
import androidx.core.view.updateLayoutParams
import com.fordownloads.orangefox.Const
import com.fordownloads.orangefox.Const.argStandalone
import com.fordownloads.orangefox.Const.extraTitle
import com.fordownloads.orangefox.R
import com.fordownloads.orangefox.RecyclerAdapter
import com.fordownloads.orangefox.RecyclerItem
import com.fordownloads.orangefox.api.DeviceRequest
import com.fordownloads.orangefox.api.RequestAPI
import com.fordownloads.orangefox.base.FragmentBase
import com.fordownloads.orangefox.databinding.FragmentDevicesBinding
import com.fordownloads.orangefox.ui.info.ActivityInfo
import com.fordownloads.orangefox.ui.main.ActivityMain
import com.fordownloads.orangefox.utils.Tools
import com.fordownloads.orangefox.utils.Tools.doYeetIfNoPM
import com.fordownloads.orangefox.utils.Tools.replace
import com.fordownloads.orangefox.utils.applyInsets
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.UnknownHostException

class FragmentDevices: FragmentBase<FragmentDevicesBinding>(R.layout.fragment_devices, skipYeet = true) {

    override fun setup(state: Bundle?) {
        applyInsets(b.root, toolbar = b.appToolbar, recycler = b.recycler, recyclerPadding = false) {
            b.search.updateLayoutParams<ViewGroup.MarginLayoutParams> {
                dp(16).let { leftMargin = it + left; rightMargin = it + right }
            }
            b.recycler.setPadding(0, dp(40), 0, bottom)
        }

        if (argStandalone) {
            b.appToolbar.setNavigationOnClickListener { activity?.onBackPressed() }
            RequestAPI.orangeFoxApi.devicesAll().enqueue(object : Callback<DeviceRequest> {
                override fun onFailure(call: Call<DeviceRequest>, t: Throwable) {
                    Tools.reportException(t, false)
                    b.errorMessage = if (t is UnknownHostException) getString(R.string.err_no_internet)
                    else t.toString()
                }

                override fun onResponse(call: Call<DeviceRequest>, response: Response<DeviceRequest>) {
                    if (response.isSuccessful)
                        response.body()?.let {
                            InitSetup.listDevices(it)?.let { devices ->
                                initAdapter(devices)
                                return
                            }
                        }

                    b.errorMessage = "${response.code()}: ${getString(R.string.err_ise)}"
                }
            })
        } else {
            b.appToolbar.setPadding(0, 0, dp(16), 0)
            b.appToolbar.navigationIcon = null
            InitSetup.devices?.let { initAdapter(it) }
        }
    }

    private fun initAdapter(devices: ArrayList<RecyclerItem>) {
        b.recycler.adapter = RecyclerAdapter(
            items = devices,
            listener = { ib, _ ->
                ib.item?.data?.let {
                    prefs.apply {
                        deviceId = it
                        deviceCodename = ib.item?.subtitle
                        deviceName = ib.item?.title
                    }
                    if (argStandalone)
                        startActivity(Intent(activity, ActivityMain::class.java)
                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                            .putExtra(Const.FORCE_RELOAD, true))
                    else
                        fm?.replace(FragmentPermissions())
                }
            },
            listenerHold = { ib, _ ->
                ib.item?.data?.let {
                    startActivity(
                        Intent(activity, ActivityInfo::class.java)
                            .putExtra(Const.EXTRA_TYPE, Const.TYPE_DEVICE_INFO)
                            .putExtra(Const.EXTRA_DEVICE_ID, it)
                            .putExtra(Const.EXTRA_TITLE, R.string.dev_info)
                    )
                }
            }
        ).also { it.setSearchField(b.search) }
    }
}