package com.fordownloads.orangefox.ui.logs

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.*
import android.widget.*
import androidx.appcompat.widget.ListPopupWindow
import com.fordownloads.orangefox.*
import com.fordownloads.orangefox.base.FragmentBase
import com.fordownloads.orangefox.databinding.FragmentLogsBinding
import com.fordownloads.orangefox.utils.Tools.activityCallback
import com.fordownloads.orangefox.utils.Tools.reportException
import com.fordownloads.orangefox.utils.Tools.share
import kotlinx.coroutines.*
import java.io.File
import java.io.IOException
import java.nio.file.Files
import java.util.*
import kotlin.concurrent.thread

class FragmentLogs : FragmentBase<FragmentLogsBinding>(R.layout.fragment_logs), AdapterView.OnItemClickListener {
    private lateinit var adapter: RecyclerAdapter
    private lateinit var popup: ListPopupWindow

    private var items: ArrayList<RecyclerItem> = ArrayList<RecyclerItem>()
    private var pos = 0

    private val removeItemCallback = activityCallback {
        if (it.resultCode == Activity.RESULT_OK) {
            if (pos == it.data?.getIntExtra("pos", -1))
                removeItem()
            else
                refreshList()
        }
    }

    override fun setup(state: Bundle?) {
        popup = ListPopupWindow(ContextThemeWrapper(activity, R.style.Popup_Fill)).apply {
            width = ListPopupWindow.MATCH_PARENT
            height = ListPopupWindow.WRAP_CONTENT
            setDropDownGravity(Gravity.CENTER)
            setAdapter(ArrayAdapter(requireActivity(), android.R.layout.simple_list_item_1,
                arrayOf(getString(R.string.open), getString(R.string.share), getString(R.string.delete))))
            setOnItemClickListener(this@FragmentLogs)
        }

        RecyclerAdapter(items,
            { ib, p ->
                pos = p
                removeItemCallback.launch(
                    Intent(activity, ActivityLog::class.java)
                        .putExtra("file", ib.item?.subtitle?:"")
                        .putExtra("pos", pos)
                )
            },
            { ib, p ->
                pos = p
                popup.apply {
                    anchorView = ib.root
                    show()
                }
            }
        ).also {
            adapter = it
            b.recycler.apply {
                adapter = it
                setHasFixedSize(true)
            }
        }

        refreshList()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.clear()
        inflater.inflate(R.menu.delete_logs, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onItemClick(p: AdapterView<*>?, v: View?, posPopup: Int, id: Long) {
        val fileName = items[pos].subtitle
        val log = File(Const.DIR_LOGS, fileName)

        when (posPopup) {
            0 -> removeItemCallback.launch(
                    Intent(activity, ActivityLog::class.java)
                        .putExtra("file", fileName)
                        .putExtra("pos", pos))
            1 -> activity?.share(prefs, fileName, log)
            2 -> if (log.delete()) removeItem() else showSnackbar(R.string.err_file_delete)
        }

        popup.dismiss()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.delete) {
            if (items.isNotEmpty()) {
                Files.walk(Const.DIR_LOGS.toPath()).forEach { it.toFile().delete() }
                if (Const.DIR_LOGS.delete()) {
                    items.clear()
                    adapter.notifyDataSetChanged()
                    b.showHelp = true
                } else
                    showSnackbar(R.string.err_logs_delete)
            } else
                showSnackbar(R.string.err_logs_deleted)
        }
        return false
    }

    private fun removeItem() {
        items.removeAt(pos)
        adapter.notifyItemRemoved(pos)
        b.showHelp = items.isEmpty()
    }

    private fun refreshList() {
        thread(true) {
            items.clear()
            try {
                Files.list(Const.DIR_LOGS.toPath()).sorted()
                    .forEach { path ->
                        path.fileName.toString().let {
                            items.add(RecyclerItem(
                                when {
                                    it == "releaseinfo.json" -> return@forEach
                                    it == "install.log" -> getString(R.string.log_type_install)
                                    it == "lastrecoverylog.log" -> getString(R.string.log_type_last)
                                    it == "post-install.log" -> getString(R.string.log_type_postinstall)
                                    it.endsWith(".zip") -> getString(R.string.log_type_zip)
                                    else -> getString(R.string.log_type_normal)
                                }, it, R.drawable.ic_commit))
                        }
                    }
            } catch (e: IOException) {
                reportException(e, true)
            }
            activity?.runOnUiThread {
                b.showHelp = items.isEmpty()
                adapter.notifyDataSetChanged()
            }
        }
    }
}