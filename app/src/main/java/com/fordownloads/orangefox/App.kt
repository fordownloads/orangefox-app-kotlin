package com.fordownloads.orangefox

import android.app.Application
import androidx.appcompat.app.AppCompatDelegate
import com.downloader.PRDownloader
import com.fordownloads.orangefox.updates.DownloadService
import android.os.StrictMode
import android.os.StrictMode.VmPolicy


class App : Application() {
    var downloadService: DownloadService? = null
    fun isDownloadServiceRunning() = downloadService != null

    override fun onCreate() {
        super.onCreate()

        when (Prefs(this).theme) {
            "night" ->
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
            "light" ->
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
        }

        StrictMode.setVmPolicy(VmPolicy.Builder().build())

        PRDownloader.initialize(applicationContext)
    }
}