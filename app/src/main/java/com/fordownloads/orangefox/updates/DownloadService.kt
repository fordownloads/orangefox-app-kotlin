package com.fordownloads.orangefox.updates

import android.app.PendingIntent
import android.app.Service
import android.content.Intent
import android.net.Uri
import android.os.IBinder
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import com.downloader.OnDownloadListener
import com.downloader.OnProgressListener
import com.downloader.PRDownloader
import com.downloader.Progress
import com.fordownloads.orangefox.*
import com.fordownloads.orangefox.BuildConfig
import com.fordownloads.orangefox.Const.extraDownloadApp
import com.fordownloads.orangefox.Const.extraRelease
import com.fordownloads.orangefox.Const.extraReleaseInstall
import com.fordownloads.orangefox.Const.extraStop
import com.fordownloads.orangefox.R
import com.fordownloads.orangefox.utils.Tools
import com.fordownloads.orangefox.utils.Tools.getORS
import com.topjohnwu.superuser.Shell
import java.io.File

class DownloadService : Service() {
    enum class DownloadStatus { OK, Error, Cancelled }

    var progressListener: ((total: Int, current: Int) -> Unit)? = null
    var completeListener: ((status: DownloadStatus, error: String?) -> Unit)? = null

    val notifyManager by lazy { NotificationManagerCompat.from(this) }

    val notifyComplete by lazy {
        NotificationCompat.Builder(this, Const.CHANNEL_DOWNLOAD_STATUS)
            .setAutoCancel(true)
            .setColor(ContextCompat.getColor(this, R.color.fox_notify))
            .setPriority(NotificationCompat.PRIORITY_MAX)
    }

    val notifyProgress by lazy {
        NotificationCompat.Builder(this, Const.CHANNEL_DOWNLOAD)
            .setOngoing(true)
            .setContentText(getString(R.string.preparing))
            .setSmallIcon(R.drawable.ic_download)
            .setPriority(NotificationCompat.PRIORITY_LOW)
            .setColor(ContextCompat.getColor(this, R.color.fox_notify))
            .setProgress(0, 0, true)
            .addAction(
                R.drawable.ic_check,
                getString(R.string.inst_cancel),
                PendingIntent.getService(
                    this, 0,
                    Intent(this, DownloadService::class.java)
                        .putExtra(Const.EXTRA_STOP, true),
                    PendingIntent.FLAG_CANCEL_CURRENT
                )
            )
    }


    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        if (intent.extraStop) {
            Log.d("OFR", "Shutting down DownloadService...")
            notifyManager.cancel(Const.NOTIFY_DOWNLOAD_FG)
            PRDownloader.cancelAll()
            stopForeground(true)
            stopSelf()
            return super.onStartCommand(intent, flags, startId)
        }

        (application as App).downloadService = this

        notifyManager.apply {
            cancel(Const.NOTIFY_NEW_UPD)
            cancel(Const.NOTIFY_DOWNLOAD_ERROR)
            cancel(Const.NOTIFY_DOWNLOAD_SAVED)
            cancel(Const.NOTIFY_DOWNLOAD_FG)
        }

        startForeground(Const.NOTIFY_DOWNLOAD_FG, notifyProgress.build())

        if (intent.extraDownloadApp)
            downloadApp()
        else
            intent.extraRelease?.let {
                downloadRelease(it, intent.extraReleaseInstall)
            } ?: run {
                notifyError(getString(R.string.notif_download_failed), "Missing Arguments") //TODO: localize
            }

        return super.onStartCommand(intent, flags, startId)
    }

    private fun createRequest(url: String, dir: String, fileName: String) =
        PRDownloader.download(url, dir, fileName)
            .setHeader("app-version", "${BuildConfig.VERSION_NAME} (${BuildConfig.VERSION_CODE})")
            .setUserAgent("OrangeFox-app")
            .build()
            .setOnCancelListener {
                Log.i("OFRService", "Download cancelled")
                completeListener?.invoke(DownloadStatus.Cancelled, null)
            }
            .setOnProgressListener(object : OnProgressListener {
                var lastMB = -1
                var total = -1
                var skipMB = 0

                override fun onProgress(progress: Progress) {
                    val current = (progress.currentBytes / 1048576).toInt()
                    if (total == -1) total = (progress.totalBytes / 1048576).toInt()

                    if (lastMB != current) {
                        if (++skipMB > 4) {
                            notifyManager.notify(
                                Const.NOTIFY_DOWNLOAD_FG,
                                notifyProgress
                                    .setProgress(total, current, false)
                                    .setContentText(getString(R.string.inst_progress, current, total))
                                    .build()
                            )
                            progressListener?.invoke(total, current)
                            Log.i("Progress", "$current/$total")
                            skipMB = 0
                        }
                        Log.i("dbg", "$lastMB = $current")
                        lastMB = current
                    }
                }
            })

    private fun downloadApp() {
        val finalFile = File(Const.DIR_FOX, "app.apk").absolutePath

        notifyProgress.setContentTitle(getString(R.string.notif_downloading_app))

        val apkUri = FileProvider.getUriForFile(
            this,
            BuildConfig.APPLICATION_ID + ".fileprovider",
            File(Const.DIR_FOX, "app.apk")
        )

        val openApk = Intent(Intent.ACTION_VIEW, apkUri).apply {
            putExtra(Intent.EXTRA_NOT_UNKNOWN_SOURCE, true)
            setDataAndType(apkUri, "application/vnd.android.package-archive")
            flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK or
                    Intent.FLAG_GRANT_READ_URI_PERMISSION
        }.toPending()

        createRequest(BuildConfig.UPDATE_URL, Const.DIR_FOX, "app.apk")
            .start(object : OnDownloadListener {
                override fun onDownloadComplete() {
                    val silentUpdateCommand = "pm install -g $finalFile && " +
                            "am start -n '${BuildConfig.APPLICATION_ID}/.activity.MainActivity'"

                    //Try update using root silently. Notify when failed/no root
                    if (!Shell.rootAccess() || !Shell.su(silentUpdateCommand).exec().isSuccess)
                        notifyManager.notify(
                            Const.NOTIFY_DOWNLOAD_SAVED,
                            notifyComplete
                                .setSmallIcon(R.drawable.ic_check)
                                .setContentTitle(getString(R.string.notif_downloaded_app))
                                .setText(getString(R.string.inst_downloaded, finalFile))
                                .setContentIntent(openApk)
                                .build()
                        )

                    completeListener?.invoke(DownloadStatus.OK, null)

                    stopForeground(true)
                    stopSelf()
                }

                override fun onError(error: com.downloader.Error?) {
                    notifyError(getString(R.string.notif_download_failed_app), getString(R.string.err_check_pm_inernet))
                }
            })
    }

    private fun Intent.toPending() =
        PendingIntent.getActivity(applicationContext, 0, this, 0)

    private fun NotificationCompat.Builder.addButton(resId: Int, type: Const.NotifyAction) =
        addAction(R.drawable.ic_check, getString(resId),
            PendingIntent.getBroadcast(
                applicationContext, 0,
                Intent(applicationContext, BroadcastReceiver::class.java)
                    .putExtra(Const.EXTRA_ACTION, type), 0
            )
        )

    private fun NotificationCompat.Builder.setText(text: String) = apply {
        setContentText(text)
        setStyle(NotificationCompat.BigTextStyle().bigText(text))
    }

    private fun downloadRelease(release: ReleaseToInstall, installRelease: Boolean) {
        val finalFile = File(Const.DIR_DOWNLOAD, release.variant.filename)

        val url = Tools.getUrlMirror(Prefs(this), release.variant.mirrors) ?: run {
            return notifyError(getString(R.string.notif_download_failed, release.version), "Mirror error") //TODO: translate
        }

        notifyProgress.setContentTitle(getString(R.string.notif_downloading, release.version))

        val notifyAction = Intent(Intent.ACTION_VIEW).apply {
            setDataAndType(Uri.fromFile(finalFile), "application/zip")
            flags = Intent.FLAG_ACTIVITY_NEW_TASK
        }.toPending()

        createRequest(url, Const.DIR_DOWNLOAD, release.variant.filename)
            .start(object : OnDownloadListener {
            override fun onDownloadComplete() {
                notifyManager.notify(
                    Const.NOTIFY_DOWNLOAD_FG,
                    notifyProgress.setContentText(getString(R.string.md5_check))
                        .setProgress(0, 0, true)
                        .build()
                )
                if (!Tools.checkMD5(release.variant.md5, finalFile))
                    return notifyError(getString(R.string.notif_download_failed, release.version), getString(R.string.err_md5_wrong))

                if (installRelease) {
                    if (Shell.su("echo 'install /sdcard/Fox/releases/${release.variant.filename}' > ${getORS()}").exec().isSuccess)
                        notifyManager.notify(
                            Const.NOTIFY_DOWNLOAD_SAVED,
                            notifyComplete
                                .setSmallIcon(R.drawable.ic_device_download)
                                .setContentTitle(getString(R.string.notify_pending_reboot, release.version))
                                .setText(getString(R.string.notify_pending_reboot_sub, finalFile))
                                .addButton(R.string.reboot, Const.NotifyAction.RebootRecovery)
                                .addButton(R.string.inst_cancel, Const.NotifyAction.DeleteORS)
                                .build()
                        )
                    else
                        return notifyError(getString(R.string.notif_download_failed, release.version), getString(R.string.err_ors, finalFile))
                } else
                    notifyManager.notify(
                        Const.NOTIFY_DOWNLOAD_SAVED,
                        notifyComplete
                            .setSmallIcon(R.drawable.ic_check)
                            .setContentTitle(getString(R.string.notif_download_complete, release.version))
                            .setText(getString(R.string.inst_downloaded, finalFile))
                            .setContentIntent(notifyAction)
                            .build()
                    )

                completeListener?.invoke(DownloadStatus.OK, null)

                stopForeground(true)
                stopSelf()
            }

            override fun onError(error: com.downloader.Error?) {
                notifyError(getString(R.string.notif_download_failed, release.version), getString(R.string.err_check_pm_inernet))
            }
        })
    }

    //getString(R.string.notif_download_failed, version)
    fun notifyError(title: String, text: String) {
        notifyManager.notify(
            Const.NOTIFY_DOWNLOAD_ERROR,
            notifyComplete
                .setSmallIcon(R.drawable.ic_warning)
                .setContentTitle(title)
                .setText(text)
                .build()
        )
        completeListener?.invoke(DownloadStatus.Error, text)
        stopForeground(true)
        stopSelf()
    }

    override fun onBind(intent: Intent): IBinder? = null
    override fun onDestroy() {
        super.onDestroy()
        progressListener = null
        completeListener = null
        (application as App).downloadService = null
    }
}