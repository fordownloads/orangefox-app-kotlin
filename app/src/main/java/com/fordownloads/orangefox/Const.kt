package com.fordownloads.orangefox

import android.Manifest
import android.content.Intent
import android.icu.util.TimeUnit.values
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.view.animation.Interpolator
import androidx.core.os.bundleOf
import androidx.core.view.animation.PathInterpolatorCompat
import androidx.fragment.app.Fragment
import com.tonyodev.fetch2.EXTRA_DOWNLOAD_ID
import java.io.File
import java.sql.Types
import java.time.chrono.JapaneseEra.values

object Const {
    val STORAGE_PM = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R)
        arrayOf(
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.MANAGE_EXTERNAL_STORAGE
        )
    else
        arrayOf(
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE
        )

    val DEVICE_CHECK = listOf(Build.DEVICE.lowercase(), Build.MODEL.lowercase(), Build.PRODUCT.lowercase())

    val INTERPOLATOR: Interpolator =
        PathInterpolatorCompat.create(.2F, .8F, 0F, 1F)

    val DIR_CACHE = File("/cache")
    val DIR_LOGS = File(Environment.getExternalStorageDirectory(), "Fox/logs")
    val DIR_DOWNLOAD: String =
        File(Environment.getExternalStorageDirectory(), "Fox/releases").absolutePath
    val DIR_FOX: String =
        File(Environment.getExternalStorageDirectory(), "Fox").absolutePath
    val DIR_FOX_MAGISK =
        File(Environment.getExternalStorageDirectory(), "Fox/FoxFiles/Magisk.zip")
    val LOG_LAST =
        File(Environment.getExternalStorageDirectory(), "Fox/logs/lastrecoverylog.log")
    val RELEASE_JSON =
        File(Environment.getExternalStorageDirectory(), "Fox/logs/releaseinfo.json")

    const val TAG = "OrangeFox"

    const val ORS_FILE = "/cache/recovery/openrecoveryscript"

    const val CHANNEL_UPDATE = "CHANNEL_UPDATE"
    const val CHANNEL_DOWNLOAD = "CHANNEL_DOWNLOAD"
    const val CHANNEL_DOWNLOAD_STATUS = "CHANNEL_DOWNLOAD_STATUS"

    const val NOTIFY_NEW_UPD = 1000
    const val NOTIFY_DOWNLOAD_FG = 3000
    const val NOTIFY_DOWNLOAD_SAVED = 4000
    const val NOTIFY_DOWNLOAD_ERROR = 5000

    const val SCHEDULER_JOB_ID = 1000
    const val ONE_DAY = 86400000L

    const val FRAGMENT_INSTALL = "INSTALL"
    const val FRAGMENT_SCRIPTS = "SCRIPTS"
    const val FRAGMENT_LOGS = "LOGS"

    const val STATE_SELECTED = "SELECTED"
    const val STATE_RESTARTED = "RESTARTED"

    const val EXTRA_RELEASE_ID = "RELEASE_ID"
    const val EXTRA_DEVICE_ID = "DEVICE_ID"
    const val EXTRA_TYPE = "TYPE"
    const val EXTRA_VERSION = "VERSION"
    const val EXTRA_RELEASE_TYPE = "RELEASE_TYPE"
    const val EXTRA_TITLE = "TITLE"
    const val FORCE_RELOAD = "FORCE_RELOAD"
    const val EXTRA_RELEASE = "EXTRA_RELEASE"
    const val EXTRA_RELEASE_INSTALL = "EXTRA_RELEASE_INSTALL"
    const val EXTRA_DOWNLOAD_APP = "EXTRA_DOWNLOAD_APP"
    const val EXTRA_ACTION = "EXTRA_ACTION"
    const val EXTRA_STOP = "EXTRA_STOP"
    val Intent.extraReleaseId get() = getStringExtra(EXTRA_RELEASE_ID)?:""
    val Intent.extraReleaseType get() = getStringExtra(EXTRA_RELEASE_TYPE)?:""
    val Intent.extraDeviceId get() = getStringExtra(EXTRA_DEVICE_ID)?:""
    val Intent.extraVersion get() = getStringExtra(EXTRA_VERSION)?:""
    val Intent.extraType get() = getIntExtra(EXTRA_TYPE, TYPE_RELEASE_ONLINE)
    val Intent.extraTitle get() = getIntExtra(EXTRA_TITLE, R.string.rel_activity)
    val Intent.extraForceReload get() = getBooleanExtra(FORCE_RELOAD, false)
    val Intent.extraRelease get() = getParcelableExtra<ReleaseToInstall>(EXTRA_RELEASE)
    val Intent.extraReleaseInstall get() = getBooleanExtra(EXTRA_RELEASE_INSTALL, false)
    val Intent.extraDownloadApp get() = getBooleanExtra(EXTRA_DOWNLOAD_APP, false)
    val Intent.extraAction get() = NotifyAction[getIntExtra(EXTRA_ACTION, 0)]
    val Intent.extraStop get() = getBooleanExtra(EXTRA_STOP, false)

    enum class NotifyAction(val action: Int) {
        Invalid(0),
        RebootRecovery(1),
        DeleteORS(2);
        companion object {
            operator fun get(value: Int) = values().firstOrNull { it.action == value }
        }
    }

    const val TYPE_RELEASE_ONLINE = 110
    const val TYPE_RELEASE_OFFLINE = 120
    const val TYPE_RELEASE_FIND_VERSION = 130
    const val TYPE_RELEASE_FIND_ID = 131
    const val TYPE_RELEASE_ALL = 140
    const val TYPE_DEVICE_INFO = 210

    const val ARG_ITEMS = "ITEMS"
    const val ARG_DIALOG = "DIALOG"

    var Fragment.argStandalone
        get() = arguments?.getBoolean("STANDALONE") ?: false
        set(value) { arguments = (arguments ?: Bundle()).apply { putBoolean("STANDALONE", value) } }
}