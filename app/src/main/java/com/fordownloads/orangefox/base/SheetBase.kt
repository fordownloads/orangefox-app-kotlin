package com.fordownloads.orangefox.base

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.updateLayoutParams
import com.fordownloads.orangefox.Const
import com.fordownloads.orangefox.R
import com.fordownloads.orangefox.databinding.SheetBaseBinding
import com.fordownloads.orangefox.utils.Tools.getScreenSize
import com.fordownloads.orangefox.utils.Tools.isClassicNavigation
import com.fordownloads.orangefox.utils.applyInsets
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import java.lang.Integer.max

class SheetBase(private val context: Context) : IBase {
    private val layoutInflater = LayoutInflater.from(context)
    val body = SheetBaseBinding.inflate(layoutInflater)
    val dialog = context.initSheet(body.root)
    private var initialized = false
    override fun dp (i: Int) = ((context.resources.displayMetrics?.density?:1F) * i).toInt()

    private fun Context.initSheet(sheetView: View): BottomSheetDialog {
        val sizes = getScreenSize()
        val card = sheetView.findViewById<View>(R.id.card)
        card.updateLayoutParams {
            width = sizes.w.coerceAtMost(sizes.h) - if (isClassicNavigation()) dp(16) else dp(32)
        }
        sheetView.y = sizes.h.toFloat()

        applyInsets(body.root) {
            body.card.updateLayoutParams<ViewGroup.MarginLayoutParams> {
                bottomMargin = max(bottom, dp(16))
            }
        }

        return BottomSheetDialog(this, R.style.SheetBackground).apply {
            setContentView(sheetView)
            dismissWithAnimation = true
            setOnShowListener {
                BottomSheetBehavior.from(
                    findViewById(com.google.android.material.R.id.design_bottom_sheet)!!
                ).setPeekHeight(sheetView.height)
            }
        }
    }

    fun show(): SheetBase {
        if (initialized)
            body.root.y = context.getScreenSize().h.toFloat()
        else
            initialized = true
        dialog.show()
        body.root.animate()
            .setInterpolator(Const.INTERPOLATOR)
            .setDuration(600).translationY(0F)
        return this
    }

    fun dismiss() = dialog.dismiss()
}