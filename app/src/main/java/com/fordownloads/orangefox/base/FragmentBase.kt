package com.fordownloads.orangefox.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.fordownloads.orangefox.Prefs
import com.fordownloads.orangefox.R
import com.fordownloads.orangefox.utils.Tools.doYeetIfNoPM
import com.fordownloads.orangefox.utils.Tools.getSnackbar

abstract class FragmentBase<VB: ViewDataBinding>(
    val layout: Int,
    val skipYeet: Boolean = false
) : Fragment(), IBase {
    internal lateinit var b: VB
    private lateinit var snackbarPlacement: View
    lateinit var prefs: Prefs
    val fm get() = activity?.supportFragmentManager

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, state: Bundle?): View {
        if (!skipYeet && requireActivity().doYeetIfNoPM())
            return View(activity)

        prefs = Prefs(activity ?: run { throw Error("Prefs creation error") })
        b = DataBindingUtil.bind(inflater.inflate(layout, container, false)) ?: run { throw Error("Fragment binding error") }
        snackbarPlacement = b.root
        setHasOptionsMenu(true)
        setup(state)
        return b.root
    }

    protected open fun setup(state: Bundle?) = Unit

    fun showSnackbar(msg: Int) = activity?.getSnackbar(msg, snackbarPlacement, true)?.show()
    fun getSnackbar(msg: Int) = activity?.getSnackbar(msg, snackbarPlacement, true)
    override fun dp (i: Int) = ((resources.displayMetrics?.density?:1F) * i).toInt()
    fun dpF (i: Int) = ((resources.displayMetrics?.density?:1F) * i)
}