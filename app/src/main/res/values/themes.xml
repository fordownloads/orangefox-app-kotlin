<resources>
    <style name="Theme" parent="Theme.MaterialComponents.DayNight.NoActionBar">
        <item name="android:windowLightStatusBar">true</item>
        <item name="android:windowLightNavigationBar">true</item>
    </style>

    <style name="Theme.OrangeFox">
        <item name="colorPrimary">@color/fox_accent</item>
        <item name="colorPrimaryVariant">@color/fox_bright</item>
        <item name="colorOnPrimary">@color/white</item>

        <item name="colorSecondary">@color/fox_accent</item>
        <item name="colorSecondaryVariant">@color/fox_accent</item>
        <item name="colorOnSecondary">@color/black</item>

        <item name="colorControlHighlight">@color/ripple</item>
        <item name="colorOnSurface">@color/google_gray</item>

        <item name="android:windowBackground">@color/fox_background</item>
        <item name="android:navigationBarColor">@color/fox_background</item>
        <item name="android:statusBarColor">@android:color/transparent</item>

        <item name="snackbarStyle">@style/Snackbar</item>
        <item name="switchStyle">@style/Switch</item>
        <item name="preferenceTheme">@style/PreferenceFragmentUI</item>
    </style>

    <style name="PreferenceFragmentUI" parent="@style/PreferenceThemeOverlay">
        <item name="android:layout">@layout/fragment_settings</item>
    </style>

    <style name="Theme.OrangeFox.Activity">
        <item name="android:statusBarColor">@color/fox_background</item>
        <item name="android:homeAsUpIndicator">@drawable/ic_arrow_back</item>
        <item name="preferenceCategoryStyle">@style/Preference.Category.Custom</item>
        <item name="preferenceStyle">@style/Preference.Custom</item>
        <item name="switchPreferenceCompatStyle">@style/Preference.Custom.SwitchPreferenceCompat</item>
        <item name="colorControlNormal">@color/google_gray</item>
        <item name="colorControlHighlight">@color/ripple</item>
    </style>

    <style name="Theme.OrangeFox.Splash">
        <item name="android:windowBackground">@drawable/splash</item>
        <item name="android:navigationBarColor">@color/fox_background</item>
    </style>

    <style name="Text">
        <item name="android:layout_width">wrap_content</item>
        <item name="android:layout_height">wrap_content</item>
        <item name="android:textColor">@color/white</item>
        <item name="android:lineSpacingMultiplier">1.1</item>
        <item name="android:textSize">15sp</item>
    </style>

    <style name="Text.Monospace">
        <item name="android:fontFamily">monospace</item>
        <item name="android:gravity">top</item>
        <item name="android:textIsSelectable">true</item>
        <item name="android:textSize">12sp</item>
    </style>

    <style name="Text.SingleLine">
        <item name="android:fontFamily">@font/euclid_flex_regular</item>
        <item name="android:ellipsize">marquee</item>
        <item name="android:fadingEdge">horizontal</item>
        <item name="android:singleLine">true</item>
        <item name="android:textSize">19sp</item>
    </style>

    <style name="Text.Subtitle">
        <item name="android:ellipsize">end</item>
        <item name="android:textColor">@color/google_gray</item>
        <item name="android:textSize">14sp</item>
    </style>

    <style name="Text.Title">
        <item name="android:textSize">21sp</item>
        <item name="android:singleLine">true</item>
        <item name="android:fontFamily">@font/euclid_flex</item>
    </style>

    <style name="Text.Title.Section">
        <item name="android:layout_marginHorizontal">24dp</item>
        <item name="android:layout_marginTop">16dp</item>
        <item name="android:layout_marginBottom">8dp</item>
    </style>

    <style name="Text.Title.Large">
        <item name="android:textSize">36sp</item>
        <item name="android:fontFamily">@font/euclid_flex_regular</item>
    </style>

    <style name="Text.Title.Small">
        <item name="android:textSize">20sp</item>
    </style>

    <style name="Text.Title.Smallest">
        <item name="android:singleLine">false</item>
        <item name="android:textSize">16sp</item>
    </style>

    <style name="Text.Gray">
        <item name="android:textSize">16sp</item>
        <item name="android:textColor">@color/google_gray</item>
        <item name="android:textAlignment">textEnd</item>
        <item name="android:paddingEnd">16dp</item>
    </style>

    <style name="Text.Bold">
        <item name="android:fontFamily">sans-serif-medium</item>
        <item name="android:gravity">center_vertical</item>
        <item name="android:paddingRight">16dp</item>
        <item name="android:textIsSelectable">true</item>
    </style>

    <style name="TableIcon">
        <item name="android:width">24dp</item>
        <item name="android:height">24dp</item>
        <item name="android:paddingRight">8dp</item>
        <item name="colorControlNormal">@color/white</item>
    </style>

    <style name="TableRow">
        <item name="android:paddingVertical">6dp</item>
    </style>

    <style name="Button">
        <item name="android:textSize">15sp</item>
        <item name="android:fontFamily">@font/euclid_flex</item>
        <item name="android:textAllCaps">false</item>
        <item name="android:letterSpacing">0</item>
        <item name="android:singleLine">true</item>
        <item name="android:stateListAnimator">@null</item>
        <item name="cornerRadius">6dp</item>
        <item name="iconGravity">textStart</item>
        <item name="iconPadding">10dp</item>
        <item name="android:paddingStart">12dp</item>
        <item name="android:paddingEnd">16dp</item>
        <item name="android:paddingVertical">7dp</item>
    </style>

    <style name="Button.Fill">
        <item name="android:textColor">@color/fox_background</item>
        <item name="iconTint">@color/fox_background</item>
    </style>

    <style name="Button.Fill.Install">
        <item name="icon">@drawable/ic_download</item>
    </style>

    <style name="Button.Outline">
        <item name="android:textColor">@color/fox_accent</item>
        <item name="iconTint">@color/fox_accent</item>
        <item name="strokeColor">@color/fox_outline</item>
        <item name="strokeWidth">1dp</item>
    </style>

    <style name="Button.Fill.Sheet">
        <item name="android:layout_width">match_parent</item>
        <item name="android:layout_height">wrap_content</item>
        <item name="cornerRadius">8dp</item>
        <item name="android:paddingVertical">10dp</item>
    </style>

    <style name="Button.Outline.Sheet">
        <item name="android:layout_width">match_parent</item>
        <item name="android:layout_height">wrap_content</item>
        <item name="cornerRadius">8dp</item>
        <item name="android:paddingVertical">10dp</item>
    </style>

    <style name="ButtonTopShape">
        <item name="cornerFamily">rounded</item>
        <item name="cornerSizeTopLeft">24dp</item>
        <item name="cornerSizeTopRight">24dp</item>
        <item name="cornerSizeBottomLeft">4dp</item>
        <item name="cornerSizeBottomRight">4dp</item>
    </style>

    <style name="Card">
        <item name="cardCornerRadius">8dp</item>
        <item name="contentPadding">0dp</item>
        <item name="android:layout_marginHorizontal">8dp</item>
        <item name="android:layout_width">match_parent</item>
        <item name="android:layout_height">wrap_content</item>
    </style>

    <style name="Card.You">
        <item name="cardBackgroundColor">@color/fox_card</item>
        <item name="cardCornerRadius">@dimen/card_radius</item>
        <item name="cardElevation">0dp</item>
        <item name="android:layout_marginHorizontal">0dp</item>
    </style>

    <style name="Card.You.Wrap">
        <item name="cardBackgroundColor">@color/fox_background</item>
    </style>

    <style name="Popup" parent="Widget.AppCompat.PopupMenu">
        <item name="popupMenuBackground">@drawable/popup_bg</item>
        <item name="android:textColorPrimary">@color/white</item>
        <item name="colorControlHighlight">@color/ripple</item>
    </style>

    <style name="Popup.Fill">
        <item name="popupMenuBackground">@drawable/popup_bg_fill</item>
    </style>

    <style name="Toolbar" parent="@style/ThemeOverlay.AppCompat.Dark.ActionBar">
        <item name="android:textColorPrimary">@color/white</item>
        <item name="colorAccent">@color/fox_accent</item>
        <item name="colorControlHighlight">@color/ripple</item>
    </style>

    <style name="ToolbarText" parent="TextAppearance.Widget.AppCompat.Toolbar.Title">
        <item name="android:textSize">21sp</item>
        <item name="android:fontFamily">@font/euclid_flex</item>
    </style>

    <style name="ToolbarStyle">
        <item name="android:theme">@style/Toolbar</item>
        <item name="android:background">@android:color/transparent</item>
        <item name="android:elevation">0dp</item>
        <item name="android:layout_width">match_parent</item>
        <item name="android:layout_height">?attr/actionBarSize</item>
        <item name="popupTheme">@style/Popup</item>
        <item name="titleTextAppearance">@style/ToolbarText</item>
        <item name="title">" "</item>
    </style>

    <style name="ToolbarStyle.Center">
        <item name="android:paddingEnd">72dp</item>
        <item name="layout_collapseMode">pin</item>
    </style>

    <style name="ToolbarStyle.Center.Back">
        <item name="navigationIcon">@drawable/ic_arrow_back</item>
    </style>

    <style name="ToolbarStyle.Center.Main">
        <item name="android:paddingEnd">18dp</item>
    </style>

    <style name="ToolbarIcon">
        <item name="android:layout_width">32dp</item>
        <item name="android:layout_height">32dp</item>
        <item name="android:layout_marginStart">24dp</item>
        <item name="android:layout_marginTop">96dp</item>
        <item name="layout_collapseMode">parallax</item>
        <item name="layout_collapseParallaxMultiplier">0.8</item>
        <item name="tint">@color/white</item>
    </style>

    <style name="ToolbarStyleLarge">
        <item name="android:layout_width">match_parent</item>
        <item name="android:layout_height">match_parent</item>
        <item name="android:background">@color/fox_background</item>
        <item name="collapsedTitleTextAppearance">@style/Text.Title</item>
        <item name="collapsedTitleGravity">center</item>
        <item name="expandedTitleMarginBottom">32dp</item>
        <item name="expandedTitleMarginEnd">24dp</item>
        <item name="expandedTitleMarginStart">24dp</item>
        <item name="expandedTitleTextAppearance">@style/Text.Title.Large</item>
        <item name="layout_scrollFlags">scroll|exitUntilCollapsed|snap</item>
        <item name="contentScrim">@color/fox_card</item>
        <item name="scrimAnimationDuration">250</item>
        <item name="scrimVisibleHeightTrigger">90dp</item>
    </style>

    <style name="ToolbarStyleLarge.Setup">
        <item name="contentScrim">@color/fox_background</item>
        <item name="scrimAnimationDuration">100</item>
        <item name="scrimVisibleHeightTrigger">200dp</item>
    </style>

    <style name="BottomNav">
        <item name="elevation">0dp</item>
        <item name="enforceMaterialTheme">true</item>
        <item name="itemRippleColor">@color/ripple</item>
        <item name="itemTextAppearanceActive">@style/BottomNavText</item>
        <item name="itemTextAppearanceInactive">@style/BottomNavText</item>
        <item name="android:background">@color/fox_background</item>
        <item name="android:minHeight">72dp</item>
        <item name="backgroundTint">@null</item>
        <item name="itemActiveIndicatorStyle">@style/BottomNavIndicator</item>
        <item name="itemIconTint">@color/bottom_nav_icon</item>
        <item name="itemPaddingBottom">14dp</item>
        <item name="itemPaddingTop">10dp</item>
        <item name="itemTextColor">@color/bottom_nav_text</item>
        <item name="itemHorizontalTranslationEnabled">true</item>
    </style>

    <style name="BottomNavIndicator">
        <item name="android:height">32dp</item>
        <item name="android:width">64dp</item>
        <item name="android:color">@color/fox_accent</item>
        <item name="marginHorizontal">4dp</item>
        <item name="shapeAppearance">@style/BottomNavIndicatorShape</item>
    </style>

    <style name="BottomNavText">
        <item name="android:textSize">13sp</item>
        <item name="android:fontFamily">@font/euclid_flex</item>
    </style>

    <style name="BottomNavIndicatorShape" parent="ShapeAppearance.MaterialComponents.SmallComponent">
        <item name="cornerSize">50.0%</item>
    </style>

    <style name="Sheet">
        <item name="android:layout_width">match_parent</item>
        <item name="android:layout_height">wrap_content</item>
        <item name="android:layout_gravity">bottom|center</item>
        <item name="cardBackgroundColor">@color/fox_card</item>
        <item name="contentPadding">0dp</item>
        <item name="contentPaddingTop">12dp</item>
        <item name="contentPaddingBottom">24dp</item>
        <item name="cardElevation">@dimen/elevation_high</item>
        <item name="shapeAppearanceOverlay">@style/SheetShape</item>
    </style>

    <style name="SheetBackground" parent="Theme.Design.BottomSheetDialog">
        <item name="bottomSheetStyle">@style/SheetBackgroundStyle</item>
        <item name="android:navigationBarColor">@android:color/transparent</item>
        <item name="enableEdgeToEdge">true</item>
    </style>

    <style name="SheetBackgroundStyle" parent="Widget.Design.BottomSheet.Modal">
        <item name="android:background">@android:color/transparent</item>
    </style>

    <style name="SheetShape">
        <item name="cornerFamily">rounded</item>
        <item name="cornerSize">24dp</item>
    </style>

    <style name="CardButtonShape">
        <item name="cornerFamily">rounded</item>
        <item name="cornerSizeTopRight">@dimen/card_radius_inside</item>
        <item name="cornerSizeTopLeft">@dimen/card_radius_inside</item>
        <item name="cornerSizeBottomRight">@dimen/card_radius</item>
        <item name="cornerSizeBottomLeft">@dimen/card_radius</item>
    </style>

    <style name="SheetListView">
        <item name="android:paddingHorizontal">26dp</item>
        <item name="android:paddingBottom">4dp</item>
    </style>

    <style name="FAB">
        <item name="android:textSize">15sp</item>
        <item name="android:fontFamily">@font/euclid_flex</item>
        <item name="android:textAllCaps">false</item>
        <item name="android:letterSpacing">0</item>
        <item name="android:singleLine">true</item>
        <item name="android:textColor">@color/fox_background</item>
        <item name="iconTint">@color/fox_background</item>
        <item name="iconPadding">12dp</item>
        <item name="android:paddingStart">16dp</item>
        <item name="shapeAppearanceOverlay">@style/FABShape</item>
    </style>

    <style name="FABShape">
        <item name="cornerFamily">rounded</item>
        <item name="cornerSize">30%</item>
    </style>

    <style name="TextBox">
        <item name="android:background">@drawable/card_background</item>
        <item name="android:linksClickable">true</item>
        <item name="android:autoLink">all</item>
        <item name="android:layout_width">match_parent</item>
        <item name="android:layout_height">match_parent</item>
        <item name="android:padding">24dp</item>
        <item name="android:fontFamily">monospace</item>
        <item name="android:gravity">top</item>
        <item name="android:textIsSelectable">true</item>
        <item name="android:textColor">@color/white</item>
        <item name="android:textSize">14sp</item>
    </style>

    <style name="TextInput" parent="Widget.MaterialComponents.TextInputLayout.OutlinedBox">
        <item name="boxCornerRadiusBottomEnd">8dp</item>
        <item name="boxCornerRadiusTopEnd">8dp</item>
        <item name="boxCornerRadiusBottomStart">8dp</item>
        <item name="boxCornerRadiusTopStart">8dp</item>
    </style>

    <style name="Search">
        <item name="layout_behavior">@string/appbar_scrolling_view_behavior</item>
        <item name="android:background">@drawable/search_box</item>
        <item name="android:hint">@string/abc_searchview_description_search</item>
        <item name="android:padding">12dp</item>
        <item name="android:paddingStart">16dp</item>
        <item name="android:gravity">top</item>
        <item name="android:drawablePadding">22dp</item>
        <item name="android:layout_marginHorizontal">16dp</item>
        <item name="android:drawableTint">@color/google_gray</item>
        <item name="android:drawableStart">@drawable/ic_search</item>
        <item name="android:layout_width">match_parent</item>
        <item name="android:layout_height">wrap_content</item>
        <item name="android:textColor">@color/white</item>
        <item name="android:textSize">18sp</item>
        <item name="android:textColorHint">@color/google_gray</item>
        <item name="android:importantForAutofill">no</item>
        <item name="android:inputType">text</item>
        <item name="layout_collapseMode">pin</item>
    </style>

    <style name="RecycleView">
        <item name="android:layout_width">match_parent</item>
        <item name="android:layout_height">match_parent</item>
        <item name="layoutManager">LinearLayoutManager</item>
        <item name="android:clipToPadding">false</item>
    </style>

    <style name="Snackbar" parent="Widget.MaterialComponents.Snackbar">
        <item name="android:layout_margin">16dp</item>
        <item name="android:paddingHorizontal">16dp</item>
        <item name="android:paddingVertical">0dp</item>
        <item name="elevation">8dp</item>
        <item name="android:background">@drawable/card_background</item>
    </style>

    <style name="Switch" parent="Widget.Material3.CompoundButton.Switch">
        <item name="android:thumb">@drawable/switch_thumb</item>
        <item name="track">@drawable/switch_track</item>
        <item name="trackTint">@color/switch_track_color</item>
        <item name="thumbTint">@color/switch_thumb_color</item>
    </style>

    <style name="Preference.Category.Custom">
        <item name="android:layout">@layout/pref_category</item>
    </style>

    <style name="Preference.Custom">
        <item name="android:layout">@layout/pref_item</item>
        <item name="allowDividerAbove">false</item>
        <item name="allowDividerBelow">false</item>
    </style>

    <style name="Preference.Custom.SwitchPreferenceCompat">
        <item name="android:widgetLayout">@layout/preference_widget_switch_compat</item>
        <item name="android:switchTextOn">@string/v7_preference_on</item>
        <item name="android:switchTextOff">@string/v7_preference_off</item>
    </style>
</resources>